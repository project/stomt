# Introduction

STOMT - The global standard for feedback

"It's like a Twitter for feedback"
"It's like a consumer-friendy GitHub Issues-System"

Collect feedback from any touchpoint and collect it on a central
but public STOMT profile. We provide you with an own feedback-standard
that makes it more simple for your users to address, literally, their wishes.
On your STOMT-profile, you can manage (label, react, assign) the feedback
to you teammates.

This Drupal module is a wrapper of the STOMT javascript widget.
This is the link to the vanilla implementation:
https://github.com/stomt/stomt-javascript-sdk

Example-implementation: https://www.stomt.com/blog

# Requirements

This module has no requirements.

# Recommended modules

No further module is recommended.

# Installation

* Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

# Configuration

Go to the module configuration form and add the username of your STOMT-page
(the one you are collecting feedback for: normally not your private
profile username)

To customize the widget:
Configuration » Web services » STOMT - The global standard for feedback

# Troubleshooting

 * If the widget does not display, check if you added your username
 to the module-settings.

# FAQ

Q: I need to place the button elsewhere. Can i do this?

A: Yes, please follow our guidelines here:
https://github.com/stomt/stomt-javascript-sdk#custom-css

Q: I want to trigger the button on my own. Can i do this?

A: Yes, you can include and configure the widget on your own:
https://github.com/stomt/stomt-javascript-sdk

# Maintainers

 Current maintainers:
 * Philipp Zentner (STOMT) - https://www.drupal.org/user/3522851
 * Max Klenk (STOMT) - https://www.drupal.org/user/3540975

 This project has been sponsored by:
  * STOMT - The global standard for feedback
    A standard and social network for feedback. Collect feedback for free.
    Visit https://www.stomt.com for more information.
    Leave us feedback here: https://www.stomt.com/stomt
    Leave us feedback for this widget here:
    https://www.stomt.com/stomt-javascript-widget
